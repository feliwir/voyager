/*
 * Copyright (C) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <adwaita.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gexiv2/gexiv2.h>

#include "voyager-window.h"

static void
app_action_help_activated (GSimpleAction *action, GVariant *parameter, gpointer app)
{
}

static void
app_action_about_activated (GSimpleAction *action, GVariant *parameter, gpointer app)
{
  static const char *authors[] = {
    "Stephan Vedder <stephan.vedder@gmail.com>",
    NULL,
  };

  gtk_show_about_dialog (gtk_application_get_active_window (GTK_APPLICATION (app)),
                         "program-name", g_get_application_name (), 
                         "version", VERSION,
                         "copyright", "Copyright \xc2\xa9 2022 Stephan Vedder",
                         "comments", _ ("An application to geotag your images"),
                         "authors", authors,
                         "logo-icon-name", APPLICATION_ID,
                         "wrap-license", TRUE,
                         "license-type", GTK_LICENSE_MIT_X11, NULL);
}

static const GActionEntry application_actions[] = {
  /* Stateless actions on the app. */
  { "help", app_action_help_activated },
  { "about", app_action_about_activated }
};

static void
startup (GtkApplication *app, gpointer user_data)
{
  g_action_map_add_action_entries (G_ACTION_MAP (app), application_actions,
                                   G_N_ELEMENTS (application_actions), app);
}

static void
activate (GtkApplication *app, gpointer user_data)
{
  g_type_ensure(VOYAGER_TYPE_WINDOW);

  VoyagerWindow *window;

  window = voyager_window_new (app);
  gtk_widget_show (GTK_WIDGET (window));
}

int
main (int argc, char **argv)
{
  AdwApplication *app;
  int             status;

  if (! gexiv2_initialize ())
    {
      return EXIT_FAILURE;
    }

  /*
   * Create a new GtkApplication. The application manages our main loop,
   * application windows, integration with the window manager/compositor, and
   * desktop features such as file opening and single-instance applications.
   */
  app = adw_application_new ("org.feliwir.voyager", G_APPLICATION_FLAGS_NONE);

  g_signal_connect (app, "startup", G_CALLBACK (startup), NULL);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);

  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
