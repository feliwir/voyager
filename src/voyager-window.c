/*
 * Copyright (C) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */

#include "voyager-window.h"
#include "voyager-exif-image.h"
#include "voyager-image-map.h"

#include <gexiv2/gexiv2.h>

struct _VoyagerWindow
{
  AdwApplicationWindow parent_instance;

  GtkWidget         *image_map;
  GtkWidget         *image_grid;
  GListStore        *image_list;
  AdwLeaflet        *leaflet;
  GtkSelectionModel *model;
};

G_DEFINE_TYPE (VoyagerWindow, voyager_window, ADW_TYPE_APPLICATION_WINDOW)

VoyagerWindow *
voyager_window_new (GtkApplication *app)
{
  return g_object_new (VOYAGER_TYPE_WINDOW, "application", app, NULL);
}

static void prev_clicked_cb (GtkWidget *widget, gpointer data);
static void activate_listitem_cb (GtkGridView *view, guint pos, GListStore *list);
static void setup_listitem_cb (GtkListItemFactory *factory, GtkListItem *list_item);
static void bind_listitem_cb (GtkListItemFactory *factory, GtkListItem *list_item);

static void
voyager_window_class_init (VoyagerWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  g_type_ensure(VOYAGER_TYPE_EXIF_IMAGE);

  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/feliwir/voyager/"
                                               "voyager-window.ui");
  gtk_widget_class_bind_template_child (widget_class, VoyagerWindow, image_map);
  gtk_widget_class_bind_template_child (widget_class, VoyagerWindow, image_grid);
  gtk_widget_class_bind_template_child (widget_class, VoyagerWindow, image_list);
  gtk_widget_class_bind_template_child (widget_class, VoyagerWindow, leaflet);
  gtk_widget_class_bind_template_child (widget_class, VoyagerWindow, model);

  gtk_widget_class_bind_template_callback (widget_class, prev_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, activate_listitem_cb);
  gtk_widget_class_bind_template_callback (widget_class, setup_listitem_cb);
  gtk_widget_class_bind_template_callback (widget_class, bind_listitem_cb);
}

void
prev_clicked_cb (GtkWidget *widget, gpointer data)
{
  GtkRoot       *root   = gtk_widget_get_root (widget);
  VoyagerWindow *window = VOYAGER_WINDOW (root);

  adw_leaflet_navigate (window->leaflet, ADW_NAVIGATION_DIRECTION_BACK);
}

void
activate_listitem_cb (GtkGridView *view, guint pos, GListStore *list)
{
  GtkRoot       *root   = gtk_widget_get_root (GTK_WIDGET (view));
  VoyagerWindow *window = VOYAGER_WINDOW (root);

  VoyagerExifImage *image = VOYAGER_EXIF_IMAGE (
      g_list_model_get_item (G_LIST_MODEL (window->image_list), pos));
  voyager_image_map_focus_image (VOYAGER_IMAGE_MAP (window->image_map), image);

  adw_leaflet_navigate (window->leaflet, ADW_NAVIGATION_DIRECTION_FORWARD);
  // adw_view_stack_set_visible_child_name(window->stack, "map");
}

void
setup_listitem_cb (GtkListItemFactory *factory, GtkListItem *list_item)
{
  GtkWidget *picture;

  picture = gtk_picture_new ();
  gtk_list_item_set_child (list_item, picture);
  gtk_widget_set_size_request (picture, 200, 200);
}

void
bind_listitem_cb (GtkListItemFactory *factory, GtkListItem *list_item)
{
  GtkWidget        *picture;
  VoyagerExifImage *exif_image;

  picture    = gtk_list_item_get_child (list_item);
  exif_image = gtk_list_item_get_item (list_item);
  gtk_picture_set_paintable (GTK_PICTURE (picture), GDK_PAINTABLE (exif_image));
}

static void window_action_file_open (GSimpleAction *action, GVariant *parameter, gpointer user_data);

static const GActionEntry window_actions[] = {
  /* Stateless actions on the window. */
  { "open", window_action_file_open },
};

static void
image_selected_cb (VoyagerImageMap *self, VoyagerExifImage *image, gpointer user_data)
{
  VoyagerWindow *window = VOYAGER_WINDOW (user_data);
  g_print ("IMAGE SELECTED\n");

  guint position;

  if (g_list_store_find (window->image_list, image, &position))
    {
      gtk_selection_model_select_item (window->model, position, false);
    }
}

static void
image_unselected_cb (VoyagerImageMap *self, VoyagerExifImage *image, gpointer user_data)
{
  VoyagerWindow *window = VOYAGER_WINDOW (user_data);
  g_print ("IMAGE UNSELECTED\n");

  guint position;

  if (g_list_store_find (window->image_list, image, &position))
    {
      gtk_selection_model_unselect_item (window->model, position);
    }
}

static void
voyager_window_init (VoyagerWindow *window)
{
  g_type_ensure (VOYAGER_TYPE_IMAGE_MAP);
  gtk_widget_init_template (GTK_WIDGET (window));

  g_action_map_add_action_entries (G_ACTION_MAP (window), window_actions,
                                   G_N_ELEMENTS (window_actions), window);

  g_signal_connect (window->image_map, "image-selected",
                    G_CALLBACK (image_selected_cb), window);
  g_signal_connect (window->image_map, "image-unselected",
                    G_CALLBACK (image_unselected_cb), window);
}

static void
on_response_cb (GtkNativeDialog *native, int response, gpointer user_data)
{
  VoyagerWindow *window = VOYAGER_WINDOW (user_data);

  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser  *chooser   = GTK_FILE_CHOOSER (native);
      GListModel      *files     = gtk_file_chooser_get_files (chooser);
      VoyagerImageMap *image_map = VOYAGER_IMAGE_MAP (window->image_map);

      for (int i = 0; i < g_list_model_get_n_items (files); i++)
        {
          GError *error = NULL;
          GFile  *file  = G_FILE (g_list_model_get_item (files, i));

          GExiv2Metadata *metadata = gexiv2_metadata_new ();
          gchar          *path     = g_file_get_path (file);

          if (! gexiv2_metadata_open_path (metadata, path, &error))
            continue; // Failed to load metadata

          VoyagerExifImage *exif_image = voyager_exif_image_new_from_file (file);

          g_list_store_append (window->image_list, exif_image);
          voyager_image_map_create_marker (image_map, exif_image);

          g_object_unref (file);
        }
    }

  g_object_unref (native);
}

static void
window_action_file_open (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
  VoyagerWindow *window;

  g_return_if_fail (VOYAGER_IS_WINDOW (user_data));

  window = VOYAGER_WINDOW (user_data);

  GtkFileChooserNative *native = gtk_file_chooser_native_new ("Open File",
                                                              GTK_WINDOW (window),
                                                              GTK_FILE_CHOOSER_ACTION_OPEN,
                                                              "_Open", "_Cancel");

  GtkFileFilter *filter = gtk_file_filter_new ();
  gtk_file_filter_add_mime_type (filter, "image/png");
  gtk_file_filter_add_mime_type (filter, "image/jpeg");

  GtkFileChooser *chooser = GTK_FILE_CHOOSER (native);
  gtk_file_chooser_add_filter (chooser, filter);

  gtk_file_chooser_set_select_multiple (chooser, true);

  g_signal_connect (native, "response", G_CALLBACK (on_response_cb), window);
  gtk_native_dialog_show (GTK_NATIVE_DIALOG (native));
}
