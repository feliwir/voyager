/*
 * Copyright (C) 2021-2022 James Westman <james@jwestman.net>, Stephan Vedder <stephan.vedder@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk/gtk.h>
#include "voyager-exif-image.h"

G_BEGIN_DECLS

#define VOYAGER_TYPE_IMAGE_MAP (voyager_image_map_get_type ())

G_DECLARE_FINAL_TYPE (VoyagerImageMap, voyager_image_map, VOYAGER, IMAGE_MAP, GtkWidget)

VoyagerImageMap *voyager_image_map_new (GtkApplication *app);

void voyager_image_map_create_marker (VoyagerImageMap *image_map, VoyagerExifImage *image);
void voyager_image_map_focus_image (VoyagerImageMap *image_map, VoyagerExifImage *image);

gboolean voyager_image_map_get_show_search_bar (VoyagerImageMap *self);
void     voyager_image_map_set_show_search_bar (VoyagerImageMap *self, gboolean show_search_bar);

G_END_DECLS
