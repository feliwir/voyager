/*
 * Copyright (C) 2021-2022 James Westman <james@jwestman.net>, Stephan Vedder <stephan.vedder@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */
#include "voyager-image-map.h"
#include "voyager-tile-source.h"

enum
{
  IMAGE_SELECTED,
  IMAGE_UNSELECTED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

struct _VoyagerImageMap
{
  GtkWidget parent_instance;

  GSettings *settings;

  ShumateSimpleMap         *map;
  ShumateMapSourceRegistry *registry;
  ShumateViewport          *viewport;

  ShumateMapLayer    *tile_layer;
  ShumateMarkerLayer *marker_layer;

  GtkStatusbar *statusbar;
  GtkSearchBar *search_bar;
};

G_DEFINE_TYPE (VoyagerImageMap, voyager_image_map, GTK_TYPE_WIDGET)

enum {
  PROP_0,
  PROP_SHOW_SEARCH_BAR,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

VoyagerImageMap *
voyager_image_map_new (GtkApplication *app)
{
  return g_object_new (VOYAGER_TYPE_IMAGE_MAP, "application", app, NULL);
}

void
voyager_image_map_create_marker (VoyagerImageMap *image_map, VoyagerExifImage *exif_image)
{
  gdouble longitude, latitude;

  if (! voyager_exif_image_try_get_longitude (exif_image, &longitude)
      || ! voyager_exif_image_try_get_latitude (exif_image, &latitude))
    {
      return;
    }

  GtkWidget *image = gtk_image_new_from_icon_name ("map-marker-symbolic");
  gtk_image_set_icon_size (GTK_IMAGE (image), GTK_ICON_SIZE_LARGE);
  ShumateMarker *marker = shumate_marker_new ();

  shumate_location_set_location (SHUMATE_LOCATION (marker), latitude, longitude);
  shumate_marker_set_child (marker, image);
  shumate_marker_set_selectable (marker, true);
  g_object_set_data (G_OBJECT (marker), "image", exif_image);

  shumate_marker_layer_add_marker (image_map->marker_layer, marker);
}

void
voyager_image_map_focus_image (VoyagerImageMap *image_map, VoyagerExifImage *exif_image)
{
  gdouble longitude, latitude;

  if (! voyager_exif_image_try_get_longitude (exif_image, &longitude)
      || ! voyager_exif_image_try_get_latitude (exif_image, &latitude))
    {
      return;
    }

  // Focus GPS coordinates
  shumate_location_set_location (SHUMATE_LOCATION (image_map->viewport), latitude, longitude);
  shumate_viewport_set_zoom_level (image_map->viewport, shumate_viewport_get_max_zoom_level (
                                                            image_map->viewport));

  // Set the status
  gtk_statusbar_push (image_map->statusbar,
                      gtk_statusbar_get_context_id (image_map->statusbar,
                                                    "imagepath"),
                      voyager_exif_image_get_filepath (exif_image));

  // Find the marker
  GList *markers = shumate_marker_layer_get_markers (image_map->marker_layer);
  ShumateMarker *marker;

  for (GList *elem = markers; elem; elem = elem->next)
    {
      marker                      = elem->data;
      VoyagerExifImage *cur_image = VOYAGER_EXIF_IMAGE (
          g_object_get_data (G_OBJECT (marker), "image"));

      // match
      if (cur_image == exif_image)
        {
          if (! shumate_marker_layer_select_marker (image_map->marker_layer, marker))
            {
              g_print ("Failed to set marker as selected\n");
            }
          break;
        }
    }
}

static void
voyager_image_map_dispose (GObject *object)
{
  VoyagerImageMap *image_map = VOYAGER_IMAGE_MAP (object);

  g_clear_object (&image_map->registry);

  GtkWidget *child;
  while ((child = gtk_widget_get_first_child (GTK_WIDGET (object))))
    gtk_widget_unparent (child);

  G_OBJECT_CLASS (voyager_image_map_parent_class)->dispose (object);
}

static void
voyager_image_map_get_property (GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
  VoyagerImageMap *self = VOYAGER_IMAGE_MAP (object);

  switch (prop_id)
    {
    case PROP_SHOW_SEARCH_BAR:
      g_value_set_boolean (value, voyager_image_map_get_show_search_bar (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
voyager_image_map_set_property (GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
  VoyagerImageMap *self = VOYAGER_IMAGE_MAP (object);

  switch (prop_id)
    {
    case PROP_SHOW_SEARCH_BAR:
      voyager_image_map_set_show_search_bar (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
search_changed_cb (GtkSearchEntry *self, gpointer data);

static void
voyager_image_map_class_init (VoyagerImageMapClass *klass)
{
  GObjectClass   *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = voyager_image_map_dispose;
  object_class->get_property = voyager_image_map_get_property;
  object_class->set_property = voyager_image_map_set_property;

  properties[PROP_SHOW_SEARCH_BAR] =
    g_param_spec_boolean ("show-search-bar",
                          "Show search bar",
                          "Show search bar",
                          TRUE,
                          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

  g_object_class_install_properties (object_class, N_PROPS, properties);


  gtk_widget_class_set_template_from_resource (widget_class,
                                               "/org/feliwir/voyager/"
                                               "voyager-image-map.ui");
  gtk_widget_class_bind_template_child (widget_class, VoyagerImageMap, map);
  gtk_widget_class_bind_template_child (widget_class, VoyagerImageMap, statusbar);
  gtk_widget_class_bind_template_child (widget_class, VoyagerImageMap, search_bar);

  gtk_widget_class_bind_template_callback (widget_class, search_changed_cb);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);

  signals[IMAGE_SELECTED] = g_signal_new ("image-selected",
                                          G_OBJECT_CLASS_TYPE (object_class),
                                          G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                          G_TYPE_NONE, 1, VOYAGER_TYPE_EXIF_IMAGE);

  signals[IMAGE_UNSELECTED] = g_signal_new ("image-unselected",
                                            G_OBJECT_CLASS_TYPE (object_class),
                                            G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                            G_TYPE_NONE, 1, VOYAGER_TYPE_EXIF_IMAGE);
}

static void
search_changed_cb (GtkSearchEntry *self, gpointer data)
{
  g_print("Search changed\n");
}

static void
marker_selected_cb (ShumateMarkerLayer *self, ShumateMarker *marker, VoyagerImageMap* imagemap)
{
  GtkWidget *image = shumate_marker_get_child (marker);
  gtk_image_set_from_icon_name (GTK_IMAGE (image), "map-photo-marker-symbolic");
  gtk_widget_add_css_class(image, "accent");

  VoyagerExifImage *exif_image = VOYAGER_EXIF_IMAGE (
      g_object_get_data (G_OBJECT (marker), "image"));
  g_signal_emit (imagemap, signals[IMAGE_SELECTED], 0, exif_image);
}

static void
marker_unselected_cb (ShumateMarkerLayer *self, ShumateMarker *marker, VoyagerImageMap* imagemap)
{
  GtkWidget *image = shumate_marker_get_child (marker);
  gtk_image_set_from_icon_name (GTK_IMAGE (image), "map-marker-symbolic");
  gtk_widget_remove_css_class(image, "accent");

  VoyagerExifImage *exif_image = VOYAGER_EXIF_IMAGE (
      g_object_get_data (G_OBJECT (marker), "image"));
  g_signal_emit (imagemap, signals[IMAGE_UNSELECTED], 0, exif_image);
}

static void
voyager_image_map_init (VoyagerImageMap *image_map)
{
  ShumateMapSource *map_source;
  g_autoptr (GBytes) bytes = NULL;
  const char *style_json;
  GError     *error = NULL;

  gtk_widget_init_template (GTK_WIDGET (image_map));

  image_map->settings = g_settings_new ("org.feliwir.voyager");

  // Shumate
  image_map->registry = shumate_map_source_registry_new_with_defaults ();
  shumate_map_source_registry_add (image_map->registry,
                                   SHUMATE_MAP_SOURCE (shumate_test_tile_source_new ()));

  bytes      = g_resources_lookup_data ("/org/feliwir/voyager/map-style.json",
                                        G_RESOURCE_LOOKUP_FLAGS_NONE, NULL);
  style_json = g_bytes_get_data (bytes, NULL);

  if (shumate_vector_renderer_is_supported ())
    {
      ShumateVectorRenderer *renderer = shumate_vector_renderer_new_full_from_url (
          "vector-tiles", "Vector Tiles", "© OpenStreetMap contributors", NULL,
          0, 5, 512, SHUMATE_MAP_PROJECTION_MERCATOR,
          "https://jwestman.pages.gitlab.gnome.org/vector-tile-test-data/"
          "world_overview/{z}/{x}/{y}.pbf",
          style_json, &error);

      if (error)
        {
          g_warning ("Failed to create vector map style: %s", error->message);
          g_clear_error (&error);
        }
      else
        shumate_map_source_registry_add (image_map->registry, SHUMATE_MAP_SOURCE (renderer));
    }

  map_source = shumate_map_source_registry_get_by_id (image_map->registry,
                                                      SHUMATE_MAP_SOURCE_OSM_MAPNIK);
  shumate_simple_map_set_map_source (image_map->map, map_source);
  image_map->viewport = shumate_simple_map_get_viewport (image_map->map);

  /*
   * Make all the bindings between widget properties and g_settings.
   */
  g_settings_bind (image_map->settings, "longitude", image_map->viewport,
                   "longitude", G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (image_map->settings, "latitude", image_map->viewport,
                   "latitude", G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (image_map->settings, "zoom-level", image_map->viewport, "zoom-level",
                   G_SETTINGS_BIND_DEFAULT | G_SETTINGS_BIND_GET_NO_CHANGES);

  /* Add the marker layers */
  image_map->marker_layer = shumate_marker_layer_new (image_map->viewport);
  shumate_marker_layer_set_selection_mode (image_map->marker_layer, GTK_SELECTION_SINGLE);

  g_signal_connect (image_map->marker_layer, "marker-selected",
                    G_CALLBACK (marker_selected_cb), image_map);
  g_signal_connect (image_map->marker_layer, "marker-unselected",
                    G_CALLBACK (marker_unselected_cb), image_map);

  shumate_simple_map_add_overlay_layer (image_map->map,
                                        SHUMATE_LAYER (image_map->marker_layer));
}

gboolean
voyager_image_map_get_show_search_bar (VoyagerImageMap *self)
{
  g_return_val_if_fail (VOYAGER_IS_IMAGE_MAP (self), FALSE);
  return gtk_search_bar_get_search_mode (self->search_bar);
}

void
voyager_image_map_set_show_search_bar (VoyagerImageMap *self, gboolean show_search_bar)
{
  g_return_if_fail (VOYAGER_IS_IMAGE_MAP (self));
  gtk_search_bar_set_search_mode (self->search_bar, show_search_bar);
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_SHOW_SEARCH_BAR]);
}
