/*
 * Copyright (C) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <gdk/gdk.h>

G_BEGIN_DECLS

#define VOYAGER_TYPE_EXIF_IMAGE (voyager_exif_image_get_type ())
G_DECLARE_FINAL_TYPE (VoyagerExifImage, voyager_exif_image, VOYAGER, EXIF_IMAGE, GObject)

VoyagerExifImage *voyager_exif_image_new_from_file (GFile *file);
void voyager_exif_image_set_from_file (VoyagerExifImage *image, GFile *file);
void voyager_exif_image_set_from_texture (VoyagerExifImage *image, GdkTexture *texture);

gboolean voyager_exif_image_try_get_latitude (VoyagerExifImage *image, gdouble *latitude);
gboolean voyager_exif_image_try_get_longitude (VoyagerExifImage *image, gdouble *longitude);
gchar* voyager_exif_image_get_filepath(VoyagerExifImage *image);

G_END_DECLS
