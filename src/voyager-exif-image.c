/*
 * Copyright (C) 2022 Stephan Vedder <stephan.vedder@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <https://www.gnu.org/licenses/>.
 */

#include "voyager-exif-image.h"

#include <gexiv2/gexiv2.h>
#include <graphene.h>
#include <gtk/gtk.h>

struct _VoyagerExifImage
{
  GObject parent_instance;

  GExiv2Orientation orientation;
  GdkTexture       *texture;
  GExiv2Metadata   *metadata;
  gchar            *filepath;
};

static void
voyager_exif_image_set_scale (GdkSnapshot *snapshot, GExiv2Orientation orientation)
{
  if (orientation == GEXIV2_ORIENTATION_HFLIP || orientation == GEXIV2_ORIENTATION_ROT_90_HFLIP)
    {
      gtk_snapshot_scale (snapshot, -1.0, 1.0);
    }
  else if (orientation == GEXIV2_ORIENTATION_VFLIP || orientation == GEXIV2_ORIENTATION_ROT_90_VFLIP)
    {
      gtk_snapshot_scale (snapshot, 1.0, -1.0);
    }
}

static void
voyager_exif_image_set_rotation (GdkSnapshot *snapshot, GExiv2Orientation orientation)
{
  switch (orientation)
    {
    case GEXIV2_ORIENTATION_ROT_90_HFLIP:
    case GEXIV2_ORIENTATION_ROT_90:
    case GEXIV2_ORIENTATION_ROT_90_VFLIP:
      gtk_snapshot_rotate (snapshot, 90);
      break;
    case GEXIV2_ORIENTATION_ROT_180:
      gtk_snapshot_rotate (snapshot, 180);
      break;
    case GEXIV2_ORIENTATION_ROT_270:
      gtk_snapshot_rotate (snapshot, 270);
      break;
    default:
      break;
    }
}

static void
voyager_exif_image_get_size (GExiv2Orientation orientation, int width, int height, int *out_width, int *out_height)
{
  if (orientation > GEXIV2_ORIENTATION_VFLIP)
    {
      *out_width  = height;
      *out_height = width;
    }
  else
    {
      *out_width  = width;
      *out_height = height;
    }
}

static void
voyager_exif_image_snapshot (GdkPaintable *paintable, GdkSnapshot *snapshot, double width, double height)
{
  VoyagerExifImage *image = VOYAGER_EXIF_IMAGE (paintable);

  gtk_snapshot_save (snapshot);

  int tex_width, tex_height;
  voyager_exif_image_get_size (image->orientation, width, height, &tex_width, &tex_height);
  // Move to center
  gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (width / 2, height / 2));
  // Handle flips
  voyager_exif_image_set_scale (snapshot, image->orientation);
  // Handle rotations
  voyager_exif_image_set_rotation (snapshot, image->orientation);
  // Move back to topleft
  gtk_snapshot_translate (snapshot, &GRAPHENE_POINT_INIT (-tex_width / 2, -tex_height / 2));
  // Draw the underlying textures
  gtk_snapshot_append_texture (snapshot, image->texture,
                               &GRAPHENE_RECT_INIT (0, 0, tex_width, tex_height));
  gtk_snapshot_restore (snapshot);
}

static GdkPaintableFlags
voyager_exif_image_get_flags (GdkPaintable *paintable)
{
  return GDK_PAINTABLE_STATIC_CONTENTS | GDK_PAINTABLE_STATIC_SIZE;
}

static int
voyager_exif_image_get_intrinsic_width (GdkPaintable *paintable)
{
  VoyagerExifImage *image = VOYAGER_EXIF_IMAGE (paintable);

  switch (image->orientation)
    {
    case GEXIV2_ORIENTATION_ROT_90_HFLIP:
    case GEXIV2_ORIENTATION_ROT_90:
    case GEXIV2_ORIENTATION_ROT_90_VFLIP:
    case GEXIV2_ORIENTATION_ROT_270:
      return gdk_texture_get_height (image->texture);
    default:
      return gdk_texture_get_width (image->texture);
    }
}

static int
voyager_exif_image_get_intrinsic_height (GdkPaintable *paintable)
{
  VoyagerExifImage *image = VOYAGER_EXIF_IMAGE (paintable);

  switch (image->orientation)
    {
    case GEXIV2_ORIENTATION_ROT_90_HFLIP:
    case GEXIV2_ORIENTATION_ROT_90:
    case GEXIV2_ORIENTATION_ROT_90_VFLIP:
    case GEXIV2_ORIENTATION_ROT_270:
      return gdk_texture_get_width (image->texture);
    default:
      return gdk_texture_get_height (image->texture);
    }
}

static void
voyager_exif_image_paintable_init (GdkPaintableInterface *iface)
{
  iface->snapshot             = voyager_exif_image_snapshot;
  iface->get_flags            = voyager_exif_image_get_flags;
  iface->get_intrinsic_width  = voyager_exif_image_get_intrinsic_width;
  iface->get_intrinsic_height = voyager_exif_image_get_intrinsic_height;
}

/* When defining the GType, we need to implement the GdkPaintable interface */
G_DEFINE_TYPE_WITH_CODE (VoyagerExifImage,
                         voyager_exif_image,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE, voyager_exif_image_paintable_init))

static void
voyager_exif_image_class_init (VoyagerExifImageClass *klass)
{
}

static void
voyager_exif_image_init (VoyagerExifImage *image)
{
  image->orientation = GEXIV2_ORIENTATION_UNSPECIFIED;
  image->metadata    = gexiv2_metadata_new ();
}

VoyagerExifImage *
voyager_exif_image_new_from_file (GFile *file)
{
  VoyagerExifImage *image;

  image = g_object_new (VOYAGER_TYPE_EXIF_IMAGE, NULL);

  voyager_exif_image_set_from_file (image, file);

  return image;
}

void
voyager_exif_image_set_from_file (VoyagerExifImage *image, GFile *file)
{
  g_return_if_fail (VOYAGER_IS_EXIF_IMAGE (image));

  g_object_freeze_notify (G_OBJECT (image));

  if (file == NULL)
    {
      g_object_thaw_notify (G_OBJECT (image));
      return;
    }

  GError *error    = NULL;
  gchar  *filepath = g_file_get_path (file);

  if (! gexiv2_metadata_open_path (image->metadata, filepath, &error))
    {
      g_object_thaw_notify (G_OBJECT (image));
      return;
    }

  image->orientation = gexiv2_metadata_try_get_orientation (image->metadata, &error);

  GdkTexture *texture = gdk_texture_new_from_file (file, &error);

  if (texture == NULL)
    {
      g_object_thaw_notify (G_OBJECT (texture));
      return;
    }

  image->filepath = filepath;
  voyager_exif_image_set_from_texture (image, texture);

  g_object_unref (texture);
  g_object_thaw_notify (G_OBJECT (image));
}

void
voyager_exif_image_set_from_texture (VoyagerExifImage *image, GdkTexture *texture)
{
  g_return_if_fail (VOYAGER_IS_EXIF_IMAGE (image));
  g_return_if_fail (texture == NULL || GDK_IS_TEXTURE (texture));

  g_object_freeze_notify (G_OBJECT (image));

  if (texture)
    g_object_ref (texture);

  if (texture)
    {
      image->texture = texture;
    }

  g_object_thaw_notify (G_OBJECT (image));
}

gboolean
voyager_exif_image_try_get_latitude (VoyagerExifImage *image, gdouble *latitude)
{
  return gexiv2_metadata_try_get_gps_latitude (image->metadata, latitude, NULL);
}

gboolean
voyager_exif_image_try_get_longitude (VoyagerExifImage *image, gdouble *longitude)
{
  return gexiv2_metadata_try_get_gps_longitude (image->metadata, longitude, NULL);
}

gchar *
voyager_exif_image_get_filepath (VoyagerExifImage *image)
{
  return image->filepath;
}