# Voyager

<img src="data/icons/org.feliwir.voyager.svg" width="128px" height="128px" />

## Screenshots

## Features
- Open any format supported by GDK pixbuf
- See which images contain EXIF GPS metadata
- Change the EXIF meta
- Beautiful & responsive UI using libadwaita

## Credits

- [libshumate](https://gitlab.gnome.org/GNOME/libshumate) developers
- Helpful GNOME community in matrix / IRC